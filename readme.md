# ROBOTS ON MARS -- ANGEL'S SOLUTION

> This is my proposed solution for the challenge "Robots On Mars" that you can find on this repo under
> [Robots On Mars Challenge](RobotsOnMarsChallenge.pdf)

## 1 - SUM UP

The solution to the challenge has 3 different pars and makes use of the following technologies:

+ **MongoDB:** For persistance.
+ **Server:** Programed with ExpressJS, making use of NodeJS ES6 modules and Mongoose library to connect with DB.
+ **SPA Web:** A Web Interface to send instructions to the server, get the corresponding response and show some nice info on the fly.



## 2 - INSTALLATION

### THE WEB SPA

The Web interface is un the folder [www](www)

The web interface consists on a simple SPA (single page application), programmed on JS, making use of WebPack to provide
modules implementations, SCSS compilation, babel transpilation and some other nice features.

It is a NPM project and we will make use of npm scripts to deploy it.

First we navigate to the corresponding folder and install the dependencies.

```
    $ cd www
    $ npm i
```

After that we just have to make use of one of this 2 scripts to deploy de App:
```
    $ npm run build:dev   // Will deploy the app for developement: Hot compilation, debug info, etc., under www/tmp/ folder
    $ npm run build:prod  // Will deploy the app for producction under www/dist/ folder
```

### THE DATABASE

The DataBase is a MongoDB server instantiated with Docker. You can see the configuration under [docker-compose.yml](server/docker-compose.yml)

We wil make use of docker-compose. You must have installed both docker & docker-compose. Navigate to server folder and start the container.

```
    $ cd server
    $ docker-compose up
```

### THE SERVER API

The server here makes all the hard work. It is an API implemented on NodeJS using ExpressJS framework.
It is entirely coded on TS and it is another NPM project. In [package.json](server/package.json) we can see
the scripts we will use to deploy our server.

First we must install the corresponding dependencies

```
    $ cd server
    $ npm i
```

Then we can start the server with one of the following:
```
    $ npm start         // Will start API listening on port 3000
    $ npm run debug     // Will start API on port 3000 with debug info con console.
```

The server will listen to both request on:
+ [localhost:3000](http://localhost:3000/)               --> Will serve static files to allow us to see the web SPA.
+ [localhost:3000/instructions](http://localhost:3000/instructions)   --> Implements the endpoint where we can send the input strings and obtain the calculated data to print our UI using a POST request, or see the databse conent using a GET request.

You can find also a POSTMAN COLLECTION to check the server API [here](server/RobotInstructionsAPI.postman_collection.json)

The server will serv our www/tmp folder if there is one, www/dist folder otherwise.

If you open the web SPA on your browser (preferibly google chrome) setting the URL [localhost:3000](http://localhost:3000/),  you will see somthing like this:

![App ScreenShot](www/src/assets/images/sample_UI.png)

## 3 - TESTING THE APP

Mocha testing framework has been implemented for the server part but it is far from complete. We have 2 scripts on server/package.json
to run the tests:
```
    $ npm run test         // Will run tests without debug information: clean output.
    $ npm run test-debug   // Will run tests with a lot of debug information on coonsole.
```

So far there are only 2 tests implemented:
+ Test for POST instructions endpoint
+ Test for GET instructions endpoint.
