/***********************************************************************************************************************
 Copyright Advanced Quality Solutions SL 2018

 Librería para registrar funciones de utilidad comunes a todos los proyectos.
 **********************************************************************************************************************/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ES6 IMPORTS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// i18n MODULES.
import i18next from 'i18next';
import backend from 'i18next-xhr-backend';
import jqueryI18next from 'jquery-i18next'
import LanguageDetector from 'i18next-browser-languagedetector';

// IMPORT JS MODULES: Import external JS libs.
// import $ from 'jquery';      // JQUERY LIB --> Imported throw ProvidePlugin within webpack.config.js
import 'bootstrap';             // Bootstrap 4 LIB (JS)

// CUSTOM JS MODULES: Import needed modules such as nav-bar.js, DTOs or other modules.
import '../inc/nav-bar';        // NAV-BAR JS

// CSS STYLE IMPORTS -- Import css files.
import 'bootstrap/dist/css/bootstrap.min.css';  // BOOTSTRAP STYLES
import "../scss/app_styles.scss";               // CUSTOM B2B APP STYLES

// Import License File so WebPack will copy to deployment folder.
import '../license/license.md';

// Import package.json to retrieve version number and output the version on console.
import package_json from '../../../package.json';
const {name, version} = package_json;


console.log(`AQS App ---- ${name}\nVersion ---- ${version}`);


// END OFF ES6 IMPORTS /////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Make jQuery $ short-cut available globally so we can use it in chrome devTools.
global.$ = $;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DEVELOPMENT ENVIRONMENT /////////////////////////////////////////////////////////////////////////////////
// The word DEVELOPMENT is replaced by WebPAck on build.
if (DEVELOPMENT) {

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I18NEXT -- INTERNATIONALIZATION & LOCALIZATION.//////////////////////////////////////////////////////////
//<editor-fold desc="INTERNATIONALIZATION & LOCALIZATION">
/**
 * https://www.i18next.com/
 *
 * i18next provides the standard i18n features such as (plurals, context, interpolation, format).
 * It gives a complete solution to localize your product from web to mobile and desktop.
 *
 * FROM JS
 * If we wanna grab a string from our JS code we can just write:
 * $ i18next.t("commonNS.welcome") --> It will returns the translation for corresponding string in the current lang.
 *
 * FROM HTML
 * From HTML we can indicate default strings and configure HTML elements to be auto-translated.
 * The key for the corresponding string must figure on a data-i18n attribute.
 * <button id="errorBtn" class="err-danger" data-i18n="index.buttons.errorBtn">Default String</button>
 *
 * It will load resources from a backend server using xhr requests.
 * @type {i18next.i18n | i18next}
 */
window.i18next = i18next;
i18next.use(backend).use(LanguageDetector).init({
    //lng: 'es', // Language to use (overrides language detection)
    fallbackLng: DEVELOPMENT ? 'dev' : 'en',
    whitelist: ['es','en', 'fr', 'pt', 'dev'],
    debug: DEVELOPMENT ? false : false,
    backend: {
        // Path where resources get loaded from. The returned path will interpolate lng, ns if provided.
        loadPath: 'assets/locales/{{lng}}/{{ns}}.json' }
}).then(function(t) {
    /* Once i18next has been initialized we are ready to go!
    * We will now load the jqueryI18next pluggin and set a listener to update all strings saved on html
    * [data-i18n] attributes on 'languageChanged' event.*/

    /**
     * JQuery i18next plugin: Simplifies i18next usage in projects built based on jquery
     */
    jqueryI18next.init(i18next, $, {
        tName: 't',                         // --> appends $.t = i18next.t
        i18nName: 'i18n',                   // --> appends $.i18n = i18next
        handleName: 'localize',             // --> appends $(selector).localize(opts);
        selectorAttr: 'data-i18n',          // selector for translating elements
        targetAttr: 'i18n-target',          // data-() attribute to grab target element to translate (if different then itself)
        optionsAttr: 'i18n-options',        // data-() attribute that contains options, will load/set if useOptionsAttr = true
        useOptionsAttr: false,              // see optionsAttr
        parseDefaultValueFromContent: true  // parses DEFAULT VALUES from content ele.val or ele.text
    });

    /** Method to update all strings */
    function updateContent() {
        $("body").localize(); // Translate strings
        $("#langSelect > button").removeClass("active").filter(`.${i18next.language}`).addClass("active"); // Set active language icon.
    }


    /** On languageChanged event */
    i18next.on('languageChanged', updateContent);

    /** Update i18next at start up */
    $(function() {updateContent()});
});


/** Datatables translation object (nothing to do with i18next */
export let dtTranslation = {
    search: "Buscar en la tabla...",
    processing: 'Cargando...',
    infoEmpty: "No se encontraron resultados.",
    zeroRecords: "No se encontraron resultados",
    emptyTable: "Tabla vacía, no se encontraron resultados.",}
// END OF INTERNATIONALIZATION & LOCALIZATION.////////////////////////////////////////////////</editor-fold>
////////////////////////////////////////////////////////////////////////////////////////////////////////////









////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GLOBAL VARIABLES: This variables will be accessible from every module.///////////////////////////////////
/**
 * This method toggles full screen mode.
 */
window.switchFullScr = function() {
    if (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}


// /GLOBAL VARIABLES ///////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////











////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DEFINE SERVER ENDPOINTS /////////////////////////////////////////////////////////////////////////////////
/**
 * To mock-up the rest server we will change the port of the requests. For instance:
 * login: `http://localhost:${muPort}/services/b2b/v2/login`,
 */
let muPort = 5050;

/**
 * This object will contain all Restful Server endpoints, and will be used all across the app any time
 * we need to send an ajax to our server. Nor hardcoded url allowed.
 * @type {{login: string}}
 */
export const RestURL = {
    saveInstructions:          `instructions`,
};

// END OF DEFINE SERVER ENDPOINTS //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SET LOADERS ON AJAX REQUEST. ////////////////////////////////////////////////////////////////////////////
// <editor-fold desc="Ajax global events listeners....">
// The loading GIF will be shown whenever an ajax request delays more than 400ms.
let loadingTimer;
// Set global headers with encoding and authorization token.
//$.ajaxSetup({
//    beforeSend: function (xhr) {
//        xhr.setRequestHeader("Accept", "application/json");
//        xhr.setRequestHeader("Content-Type", 'application/json; charset=utf-8');
//        xhr.setRequestHeader("Authorization", 'Bearer ' + window.sessionStorage.token);
//    }
//});

// GLOBAL AJAX START.
$(document).ajaxStart(function () {
    // Show loader on UI.
    loadingTimer = setTimeout(() => $("#loading").show(), 100);
});

// GLOBAL AJAX STOP.
$( document ).ajaxStop(function() {
    if(loadingTimer) clearTimeout(loadingTimer);
    $("#loading").hide();
});

// GLOBAL AJAX ERROR.
$( document ).ajaxError(function( event, jqXHR, settings, thrownError ) {
    // If any ajax throws an "unauthorized" error, it means session has expired
    // so we redirect to login page.
    // if(jqXHR.status == 401) {
    //     window.location.replace("login.html");
    // }
});
//////////////////////////////////////////////////////////////////////////////////////////////</editor-fold>
////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ERRORS / WARNINGS / INFO ////////////////////////////////////////////////////////////////////////////////
//<editor-fold>
/**
 * This enumeration contains all possible error levels for showMsg() method. Each of them will style
 * the modal on a different way.
 * @type {string[]}
 */
const levelsArray = ["error", "warning", "info", "success"];
/** This var will be used to manage the case when we try to show the modal and there already is another
 * instance being shown,
 * @type {boolean}
 */
let modalIsOpen = false;
/**
 * This method shows an error modal when called anywhere within the app. The #msg-modal div is included
 * in footer.html
 *
 * We will implement a custom event named customEvent:modalClosed to avoid showing the dialog if there is already
 * a previous instance on screen. In that case we will wait until we hide the existing instance to re-launch the
 * modal triggering the custom event on the closeMsg() method.
 *
 * If there is a callback method, the modal will be kept open if the modal returns false. It will be closed after
 * executing callback method otherwise.
 *
 * @param {string}              msg         - Message to be shown. Accepts HTML format.
 * @param {string}              title       - Text to be used as title.
 * @param {string}              errorLevel  - ["error" | "warning" | "info" | "success"]
 * @param {function}            [callback]  - Callback function to be executed on click on confirm button.
 * @param {(object|string)}     [options]   - Callback function parameters.
 *
 *
 * CONFIGURATION OPTIONS
 *
 * + errorLevel --
 *     The whole modal will be styled depending on the content of this parameter.
 *     The possible values will be ["error" | "warning" | "info" | "success"], and the modal will be styled on
 *     [ red | yellow | blue | green] by the css styles sheets. If errorLevel is not present default color will be grey.
 *
 * + callback --
 *
 *     If callback parameter is null the confirmation button will not be shown.
 *
 * + options.doNotClose --
 *
 *     If there is a property inside the callback options parameter called doNotClose === true, the callback will
 *     be executed on click on CONFIRM button, but the modal will not be automatically closed.
 *
 *  + options.hideConfirmBtn --
 *
 *    If there is a property inside the callback options parameter named hideConfirmBtn === true then the confirm
 *    button will not be shown and the callback will be executed when the modal is closed.
 *
 *  + options.forceCallback --
 *
 *    If there is a property inside the callback options parameter named forceCallback === true the callback will
 *    be triggered always, that means, both when we click CONFIRM button or whenever we close the modal.
 *
 *  + options.blockUI --
 *
 *    This property will have priority over all the rest and will remove both the close button and the confirm button
 *    and it will not be possible to close the modal. The UI will remain blocked.
 */
export const showMsg = function (msg, title = null, errorLevel = null, callback = null, options = {}) {

    /** Our message modal div */
    let $msgModal = $("div#msg-modal");

    /**
     * This method will just show the message modal with the corresponding configuration options, that is:
     *  + A listener will be set so the msg modal will be closed if we click anywhere outside it.
     *  + The msg modal will be styled depending on errorLevel parameter.
     *  + Both msg and title params will be set on #msg-body and #msg-title as its html content.
     *  + The CONFIRM button will only be shown if there is a callback parameter not null.
     *  + The click on the CONFIRM button will only close the modal if options.doNotClose is not set to true.
     */
    function show() {
        // Set modal is currently being shown on screen.
        modalIsOpen = true;
        // Set a flag to check if the callback was executed. We only want it to execute once.
        let callbackCalled = typeof callback !== "function";
        // Callback result: We will pick the callback function result and will keep the modal open once executed if it is false.
        let callbackResult = null;
        // Set On Click Outside listener.
        onClickOutside($msgModal.find("#msgContainer"),$msgModal, closeMsg);
        // Set title
        if (title) {
            $msgModal.find("#msg-title").html(title);
            $msgModal.attr("title", title);
        }
        // Set message.
        $msgModal.find("#msg-body").html(msg);
        // Set styles depending on errorLevel parameter.
        if (levelsArray.includes(errorLevel)) $msgModal.addClass(errorLevel);
        // Set close button click listener.
        $msgModal.find("#clsMsgBtn").on("click", closeMsg);

        // SHOW THE MODAL ON SCREEN: Focus input if there is any inside $msgModal
        $msgModal.fadeIn(300, () => $msgModal.find("input") ? $msgModal.find("input").trigger("focus") : null);

        // Confirm Button:
        let $confirmBtn = $msgModal.find("#confirmBtn");
        // Make sure the callback is a function: In case there is a callback we want it to be executed
        // both if we click confirm button or by clicking close button only if options.forceCallback == true
        if (typeof callback === "function") {
            if (options == null || options.hideConfirmBtn == null || !options.hideConfirmBtn) {
                $msgModal.find("#confirmBtn").fadeIn(200);
                // Call it on click on confirm button., since we have confirmed it is callable
                $confirmBtn.on("click", function()  {
                    // Execute call callback method with parameters, if it wasn't called before.
                    // console.log("Executing showMsg() callback.");
                    if (!callbackCalled || !callbackResult) callbackResult = callback(options);
                    callbackCalled = true;
                    // Close the modal on click on confirm if options.doNotClose == null || false and callback returned != false
                    if (callbackResult !== false && (options == null || options.doNotClose == null || !options.doNotClose)) closeMsg();
                });
            } else if (options.hideConfirmBtn) $msgModal.find("#confirmBtn").hide();
            // Also call callback it if the modal is closed and options.forceCallback == true (if it wasn't called before).
            if (options.forceCallback) $(document).one("customEvent:modalClosed", () => {if (!callbackCalled) callback(options)});

        } else $msgModal.find("#confirmBtn").hide();
    }

    // We check if the modal is already open. If it is will set a listener to show it when the current modal on screen
    // will be hidden.
    if (modalIsOpen) {
        $(document).on("customEvent:modalClosed", () => {
            // Remove the custom event listener.
            $(document).off("customEvent:modalClosed");
            // Show our message modal.
            show();
        })
    // If it is not being shown we will just show it.
    } else show();

    // If options.blockUI == true then we will block UI not allowing the user to close the modal.
    if(options !== null && options.hasOwnProperty("blockUI") && options.blockUI) {
        // Remove close modal on click outside listener.
        offClickOutSide($msgModal);
        // Remove buttons so they can't be clicked.
        $msgModal.find("#confirmBtn").add("#clsMsgBtn").hide();
    }

};
/**
 * This method closes the message shown by showMsg() and retrieves it to its initial state. It will
 * also triggers an event "customEvent:modalClosed" in case there is another called to showMsg() waiting
 * to be shown.
 */
export const closeMsg = function ()     {
    let $msgModal = $("div#msg-modal");
    $msgModal.addClass('byebye').fadeOut(400, function() {
        $(this).removeClass('byebye');
        $msgModal.find("#clsMsgBtn").add("#msgContainer").add("div#msg-modal").add("button#confirmBtn").off("click");
        $msgModal.removeClass(levelsArray.join(' '));
        $msgModal.find("#msg-title").html("");
        $msgModal.removeAttr("title");
        $msgModal.find("#msg-body").html("");
        $msgModal.find("#confirmBtn").hide(0, () => {
            modalIsOpen = false;
            $(document).trigger("customEvent:modalClosed");
        });
    });
};
global.showMsg = showMsg;

// Add a listener to close the modal on presson ESC key.
$(document).keyup(function(e) {if (e.key === "Escape") {closeMsg()}});
//</editor-fold>
// END OF ERRORS / WARNINGS / INFO /////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COMMON USE METHODS //////////////////////////////////////////////////////////////////////////////////////

/**
 * Generates a random color.
 * @returns {string}
 */
export function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}




/**
 * This method triggers a callback when $container is clicked but not $box, that is,
 * when we click outside the $box element.
 *
 * @param $box
 * @param $container
 */
export const onClickOutside = function($box, $container, callback)    {

    $container.off("click.Bst").on("click.Bst", function(event){
        if (
            $box.has(event.target).length == 0 //checks if descendants of $box was clicked
            &&
            !$box.is(event.target) //checks if the $box itself was clicked
        ){
            // console.log("You clicked outside the box");
            callback(event);
        } else {
            // console.log("You clicked inside the box");
        }
    });

};

