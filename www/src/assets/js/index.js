// Each specific JS will require common.js that includes JS and CSS libraries as well as custom JS and CSS.
import * as common from "./common";

import {RestURL, getRandomColor} from "./common";



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DOCUMENT READY BLOCK ////////////////////////////////////////////////////////////////////////////////////////////////
jQuery( document ).ready( function($) {

    // Hide legend at startup
    $("#legend").hide();

    // Set initial orders example
    $("#orders").val(`5 3\n1 1 E\nRFRFRFRF\n3 2 N\nFRRFLLFFRRFLL\n0 3 W\nLLFFFLFLFL`);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SET INDEX CONTENTS //////////////////////////////////////////////////////////////////////////////////
    //<editor-fold desc="Inject content on index.html depending on the current user.">

    /**
     *
     * Fill UI from data received from server.
     * @param data
     */
    function initPlanet(data) {

        // Set columns and rows to draw the planet grid according to data.planetWidth & data.planetHeight
        document.getElementById("planetGrid").style.setProperty('--planet-width', data.planetWidth + 1);
        document.getElementById("planetGrid").style.setProperty('--planet-height', data.planetHeight + 1);

        // Firstly we empty the planet grid.
        $("#planetGrid").html("");

        // Fill the Grid
        for (let i = 0; i < (data.planetWidth + 1); i++) {
            for (let j = 0; j < (data.planetHeight + 1); j++) {
                $("#planetGrid").append(`<div class="position" data-coord="${i}_${j}" style="grid-column: ${i+1}; grid-row: ${j+1}"></div>`)
            }
        }

        // Draw scents
        data.scents.forEach( pos => {
            let $position = $(`.position[data-coord=${pos._x}_${pos._y}]`);
            $position.addClass("hasScent");
            if (pos._x === 0) $position.addClass("left");
            if (pos._y === 0) $position.addClass("bottom");
            if (pos._x === data.planetWidth) $position.addClass("right");
            if (pos._y === data.planetHeight) $position.addClass("top");
        })

        // Draw robots trajectories and robots last positions.
        data.infoPerRobot.forEach( robot => {
            let robotColor = getRandomColor();
            let trajectory = robot.exploredPosition;
            trajectory.forEach((pos, index) => {
                let $position = $(`.position[data-coord=${pos._x}_${pos._y}]`);
                $position.addClass("explored").css("background-color", robotColor);
                if (index === trajectory.length -1 && !robot.isLost) $position.append(`<div class="robot" style="background-color: ${robotColor}"></div>`)
            });
        });

        // Draw onTheFly info.
        $("#onTheFly").empty().append(`
                    <span class="label">Planet Size</span><span class="value">${data.planetWidth} x ${data.planetHeight} units</span>
                    <span class="label">Planet Area</span><span class="value">${(data.planetWidth + 1) * (data.planetHeight + 1)} units<sup>2</sup></span>
                    <span class="label">Explored Area</span><span class="value">${$(".explored").length} units<sup>2</sup></span>
                    <span class="label">Un-Explored Area</span><span class="value">${$(".position:not(.explored)").length} units<sup>2</sup></span>
                    <span class="label">Num Explorers</span><span class="value">${data.infoPerRobot.length} robots</span>
                    <span class="label">Remaining Robots</span><span class="value">${$(".robot").length} robots</span>
                    <span class="label">Lost Robots</span><span class="value">${data.infoPerRobot.reduce((counter, robot) => robot.isLost ? counter += 1 : counter, 0)} robots</span> `);
    }

    // END OF SET INDEX CONTENTS /////////////////////////////////////////////////////////////</editor-fold>
    ////////////////////////////////////////////////////////////////////////////////////////////////////////






    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // EVENTS //////////////////////////////////////////////////////////////////////////////////////////////
    //<editor-fold desc="Define events listeners.">

    document.getElementById("runOrders").onclick = () => {


        // calculateOutPut()
        // console.log("====================================================================================")


        $.ajax({
            type: "POST",
            url: RestURL.saveInstructions,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            dataType: 'json',
            data: {input: $("#orders").val()}
        }).done(data => {
            $("#results").val(data.output); // Show output on UI.
            initPlanet(data); // Draw results.
            $("#legend").fadeIn();
        }).fail(err => showMsg(err.responseJSON.error, "Ups...:(", "warning"));

    };




    // ENF OF EVENTS /////////////////////////////////////////////////////////////////////////</editor-fold>
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

});
// END OF DOCUMENT READY BLOCK /////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////