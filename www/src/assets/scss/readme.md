# AQS - Guía de Buenas Prácticas CSS

En el mundo del CSS, documentación es subutilizada. Puesto que la documentación no es visible para el usuario final, su valor es a menudo pasado por alto por los clientes.

## 1 - Estructura del Código.

El código SCSS está estructurado en archivos separados siguiendo la siguiente estructura.

```text
scss/
├── app_styles.scss                 // Estilos particulares de la APP.
├── base
│   └── _reset.scss                 // Estilos básicos generales (ej: body, h1, h2, h3...)
├── components                      // ESTILOS DE LOS COMPONENTES:                 
│   ├── _buttons.scss               // Estilos de los botones.
│   ├── _form.scss                  // Estilos de los formularios.
│   ├── _modals.scss                // Estilos de los modales.
│   ├── _tables.scss                // Estilos de las tablas.
│   └── _tabs.scss                  // Estilos de los tabs.
├── main.scss                       // Compilación de los estilos generales: base y components.   
├── readme.md                       // Documento de buenas prácticas.
└── variables                       // VARIABLES
    └── _variables.scss             // Variables CSS: Colores.
```

## 2 - Importando Estilos.

En las aplicaciones web de AQS se utilizará como framework de estilos por normal general Bootstrap 4.0 (o mayor).

Los estilos de bootstrap así como el código JS se encuentran en node_modules, y se importarán como un módulo ES6 desde el archivo commons.js o desde el módulo de la seccion correspondiente (login, index, etc...) si fuera necesario.

```javascript
// IMPORT JS MODULES:
import 'bootstrap';                             // Bootstrap 4 LIB (JS)
// CSS STYLE IMPORTS -- Import css files.
import 'bootstrap/dist/css/bootstrap.min.css';  // BOOTSTRAP STYLES
import "../scss/main.scss";                     // CUSTOM MAIN CASTOR WEB STYLES
import "./app_styles.scss";               // CUSTOM B2B APP STYLES
```

A su vez deben importarse siempre main.scss y el archivo de estilos particulares de la app correspondiente, app_styles.scss

En el proceso de compilación de la App, hecho con WebPack 4, y configurado en webpack.config.js, todos los estilos importados se compilarán en un único archivo styles.[hash].css, y dicho archivo se linkará automáticamente al html de cada sección en la cabecera.


## 3 - CSS Specificity

Specificity determines which CSS property declaration is applied when two or more declarations apply to the same element with competing property declarations. The most specific selector takes precedence. If specificity is equal, the last declaration in the source order takes precendence.By understanding CSS Specificity, you’ll understand how the browser determines which declaration is more specific and will therefore take precedence

### Selector Specificity Weights

* **!important:** Any property declaration with the term !important takes highest precendence, even over inline styles. If !important is declared more than once on conflicting properties targeting the same element, you CSS author be shot, and the other precendence rules are in effect. It’s as if the weight of the selector with the !important declaration were 1-X-A-B-C, for that property only (where A, B and C are the actual values of the parent selector as described below). Because of this, important should not be used, but can be handy in debugging.

* **style=””:** If the author includes style attribute on an element, the inline style will take precedence over any styles declared in an external or embedded stylesheet, other than !important declarations. Anyone including style in production should also be shot, Just sayin’. Note that styles added with the JavaScript style (i.e. document.getElementById('myID').style.color = 'purple';) property is in effect creating an inline style ( ... id="myID" style="color: purple"...). The weight of style=”” is 1-0-0-0.

* **id:** Among the selectors you’ll actually be including in your stylesheet, ID’s have the highest specificity. The weight of an id selector is 1-0-0 per id.

* **class/pseudo-class/attributes:** As shown in the second code block above, class selectors, attribute selectors and pseudo-classes all have the same weight of 0-1-0.

* **type:** Element type selectors and pseudo-elements, like :first-letter or :after have the lowest value in terms of specificity with 0-0-1 per element.


If more than one selector have the same specificity, then adhere to the cascade: the last declared rule takes precedence.




## 4 - Estándares de Codificación

### General Rules

* Configure your editor to "show invisibles". This will allow you to eliminate end of line whitespace, eliminate unintended blank line whitespace, and avoid polluting commits. On WEBStorm it is done under:

        Settings -> Editor -> General -> Appearance -> Show whitespaces

* Don’t use tabs for indentation. Do use four **(4) spaces** for indentation

* Use separate files (concatenated by a build step) to help break up code for distinct components.

* As a rule of thumb, don't nest further than 3 levels deep. If you find yourself going further, think about reorganizing your rules (either the specificity needed, or the layout of the nesting)

* Try not to use tag styling, but use classes instead. If you do style using a tag, like \<h3\>, make it for general purpose, and be sure you only use it once in the whole css.

* Encourage the use of SCSS Mixins to define general styles: It allow us to re-use styles, properties and selectors without having to copy/paste them  
  
        .btn-a {
           background: blue;
           color: white;
           width: 30px;
           border-radius: 10px;
        }
        .btn-b {
           background: black;
           color: white;
           width: 30px;
           border-radius: 10px;
        }
  Becomes:
  
        @Mixin botones {
           color: white;
           width: 30px;
           border-radius: 10px;
        }
        .btn-a {
           @include botones;
           background: blue;
        }
        .btn-b {
           @include botones;
           background: black;
        }
  

* **About Naming I:** Use ID and class names that are as short as possible but as long as necessary.

        E.g. #nav not #navigation, .author not .atr
        
  Don’t use Class and ID names to reflect the underlying markup structure. .container-span and .small-header-div are bad names. Do think about CSS in terms of objects and use simple nouns as names. .global-alert and .badge are good names.

* **About Naming II:**  Do not concatenate words and abbreviations in selectors by any characters (including none at all) other than hyphens, in order to improve understanding and scannability.
  
        E.g. .demo-image not .demoimage or .demo_image

* Try to never use IDs to add css styles. There is literally no point in them, and they only ever cause harm. it has a quasi-!important effect on elements. If you use #id then you can’t override that elements style unless you reference it with #id again.
  
  Do use classes to facilitate reusability and reduce CSS selector specificity conflicts.

        #nav .logo {}
        .logo {}             /* #nav ignores .logo */
        
        .nav .logo {}
        .logo {}             /* .nav obeys .logo */


* In situations where it would be useful for a developer to know exactly how a chunk of CSS applies to some HTML, include a snippet of HTML in a CSS comment.
  
        /*--------------------------------------------------------------------*\
            $TOOLTIPS
            
            <small class=tooltip><span>Lorem ipsum dolor</span></small>
        \*--------------------------------------------------------------------*/
        .tooltip{
            [styles]
        }
            .tooltip > span{
                [styles]
            }

* **Indenting rulesets:** Indent your rulesets to mirror the nesting of their corresponding HTML, as far as you can. For example, take this carousel HTML:
        
        /* CARROUSEL */
        <div class=carousel>
            
            <ul class=panes>
                
                <li class=pane>
                    <h2 class=pane-title>Lorem</h2>
                </li><!-- /pane -->
                
                <li class=pane>
                    <h2 class=pane-title>Ipsum</h2>
                </li><!-- /pane -->
                
                <li class=pane>
                    <h2 class=pane-title>Dolor</h2>
                </li><!-- /pane -->
                
            </ul><!-- /panes -->
            
        </div><!-- /carousel -->

  Your CSS for this would be formatted: 
  
        /*--------------------------------------------------------------------*\
            $CAROUSEL
        \*--------------------------------------------------------------------*/
        .carousel{
            [styles]
        }
        
            .panes{
                [styles]
            }
        
                .pane{
                    [styles]
                }
        
                    .pane-title{
                        [styles]
                    }
        /*--------------------------------------------------------------------*\
            END OF $CAROUSEL
        \*--------------------------------------------------------------------*/
            

* **About Units**: Units in **'rem'** preferred because it does not inherit a percentage value of its parent element, but instead is based on a multiplier of the root font-size.

* Long, comma-separated property values - such as collections of gradients or shadows - can be arranged across multiple lines in an effort to improve readability and produce more useful diffs.
  
* Write multiple selectors on separate lines.

        .btn,
        .btn-link {
        }
  
* Include one space between the selector and the opening brace.

        .Selector {
        }
        
* Use shorthand for hex values when possible. Write hex values in lowercase.

        #fff vs #ffffff
        #3d3d3d vs #3D3D3D

* Enclose URLs in single quotes. Generally, **single quotes are preferred over double quotes**, since they’re easier to type.
  
        url ('/image.png'); vs url ("/image.png");
  
* Don’t use units for zero (0) values, except for angles (deg) and time (s or ms).      
  
        margin-right: 0; vs margin-right: 0px;
  
* **Vendor prefixes:** Write vendor prefixes so that the values all line up vertically; this means YOU can scan them quicker (to check they’re all identical) and you can alter them all in one

        .island{
            padding:1.5em;
            margin-bottom:1.5em;
            -webkit-border-radius:4px;
               -moz-border-radius:4px;
                    border-radius:4px;
        }

* Follow the structure as you add styles to your app. Throwing every new style you create onto the end of a single file would make finding things more difficult and would be very confusing for anybody else working on the project.
  

  
### App Styles: Sections


* **Section Titles:** Denote each section (layout, type, tables etc) of the CSS thus:

```
/*----------------------------------------------------------------------------*\
    $LOGIN
\*----------------------------------------------------------------------------*/
    
    [STYLES]
    
/*----------------------------------------------------------------------------*\
    END OF $LOGIN SECTION
\*----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*\
    $INDEX
\*----------------------------------------------------------------------------*/
    
    [STYLES]
    
/*----------------------------------------------------------------------------*\
    END OF $INDEX SECTION
\*----------------------------------------------------------------------------*/
```
  Include 5 line breaks between sections so the can be easily localized.

* **Block Titles:** Denote every block inside each section with openning/close comments like:
```
/*----------------------------------------------------------------------------*\
    $LOGIN
\*----------------------------------------------------------------------------*/
   
    /*----------------------------------------------------------------*\
        $LOGIN: LOGIN FORM
    \*----------------------------------------------------------------*/
       
        ...
        
    /*----------------------------------------------------------------*\
        END OF $LOGIN: LOGIN FORM
    \*----------------------------------------------------------------*/
    
    
    
    /*----------------------------------------------------------------*\
        LOADER
    \*----------------------------------------------------------------*/
           
        ...
            
    /*----------------------------------------------------------------*\
        END OF LOADER
    \*----------------------------------------------------------------*/
    
/*----------------------------------------------------------------------------*\
    END OF $LOGIN SECTION
\*----------------------------------------------------------------------------*/
```
  Include 3 line breaks between blocks so the can be easily localized.


* Make use of the folding tag <editor-fold> on WEBStorm so we can collapse and expande our CSS code for easier visualization and management. Use also the desc attribute inside the <editor-fold desc="description> to describe the code between the folding tags.
```
/*----------------------------------------------------------------------------*\
    $LOGIN
\*----------------------------------------------------------------------------*/
// <editor-fold desc="Login section styles.">
   
    /*----------------------------------------------------------------*\
        $LOGIN: LOGIN FORM
    \*----------------------------------------------------------------*/
    // <editor-fold desc="Form Login.">   
        ...
        
    /*--------------------------------------------------*\</editor-fold>
        END OF $LOGIN: LOGIN FORM
    \*----------------------------------------------------------------*/
    
    
    
    /*----------------------------------------------------------------*\
        LOADER
    \*----------------------------------------------------------------*/
    // <editor-fold desc="Specific loader for login section.">       
        ...
            
    /*--------------------------------------------------*\</editor-fold>
        END OF LOADER
    \*----------------------------------------------------------------*/
    
/*--------------------------------------------------------------*\</editor-fold>
    END OF $LOGIN SECTION
\*----------------------------------------------------------------------------*/
```


* Use comments to decompose the html/css structure. For instance, if we want to give styles to a grid of cards. Some of then will be standard, but there will also be some cards shown on a list and some of then on a dark them. We can do:

```
    /*----------------------------------------------------------------*\
        CARDS GRID
    \*----------------------------------------------------------------*/
    // <editor-fold desc="Specific loader for login section.">       
        
        /**
         * CARDS GRID
         *
         * These are the styles for the card component.
         *
         * Index
         * - Card Base 
         * - Card Grid
         * - Card List
         * - Card Dark
         */
         
         
        /**
         * Card Base
         */
         
         
        /**
         * Card Grid
         */
         
         
        /**
         * Card List
         */
         
         
        /**
         * Card Dark
         */
    /*--------------------------------------------------*\</editor-fold>
        END OF CARDS GRID
    \*----------------------------------------------------------------*/
```


* Add line comments to items style when necessary. Comments that refer to selector blocks should be on a separate line immediately before the block to which they refer to.

```
    /*----------------------------------------------------------------*\
        CARDS GRID
    \*----------------------------------------------------------------*/
    // <editor-fold desc="Specific loader for login section.">       
        
        /**
         * Card Base
         *
         * Defines the appearance and behaviour of the default card with its main parts:
         * - Card Image
         * - Card Content
         * - Card Footer
        */
         
        .card {...}
        .card__image {...}
        .card__logo {...}
        .card__content {...}
        .card__footer {...}
            
    /*--------------------------------------------------*\</editor-fold>
        END OF CARDS GRID
    \*----------------------------------------------------------------*/
```


* Add rule specific comments when needed.

```
    /*----------------------------------------------------------------*\
        LOADER
    \*----------------------------------------------------------------*/
    // <editor-fold desc="Specific loader for login section.">       
        
        /**
         * Card Base
         *
         * Defines the appearance and behaviour of the default card with its multiple parts:
         * - Card Image
         * - Card Logo
         * - Card Content
         * - Card Footer
        */
         
        .card {
          @media (max-width: {{ screen-xs }} ) {
            border: none;   /* because the card takes 100% of the screen on mobile */
          }
        }
        .card__image {...}
        .card__logo {...}
        .card__content {
          flex: 1 1 auto;   /* "auto" needs to be explicit to avoid the div collapsing on IE.*/
        }
            
    /*--------------------------------------------------*\</editor-fold>
        END OF LOADER
    \*----------------------------------------------------------------*/
```

## 5 - Taking it a Step Further
With these best practices in mind, the next step is to incorporate a living style guide as part of your documentation. A living style guide is a living document that shows the comments that you have included in your code structured like a website, so you can navigate the documentation separately from the source code.

What makes living style guides powerful is that the actual documentation lives with the code and can be easily updated as your code changes, allowing it to stay in sync and relevant. The additional advantage is that you can make this documentation available to other people on your team who might not be interacting directly with the code (like designers, product managers, or QA engineers). These team members would also find it helpful to know how the UI is shaping up.

In a living style guide, you can include interactive demonstrations of your code and you can further organize your documentation independently from the code structure.

## 6 - Conclusion
Documenting CSS begins with clear rules and a well-structured code base. When done as part of your workflow, it can also serve as a guide to structure your code and keep it organized as it grows. This has the additional benefit of making it clear where things are and where new code should be added, easing the onboarding of new members while fast-tracking development.
