/** ********************************************************************************************************************
 *  WEBPACK CONFIGURATION FILE:
 *
 *  Most important things to know about the Webpack configuration file are:
 *
 * + Entry: (optional) it’s our main Javascript file where all of the application’s code gets imported
 *
 * + Output: (optional) it’s the resulting Javascript file, bundled by Webpack
 *
 * + Module and Rules: it’s the place where you configure the LOADERS.
 *
 *   Loaders work at the individual FILE LEVEL during or before the bundle is generated.
 *
 *   Loaders do the pre-processing transformation of virtually any file format when you use sth like
 *   require("my-loader!./my-awesome-module") in your code.
 *   Compared to plugins, they are quite simple as they (a) expose only one single function to webpack and (b) are not
 *   able to influence the actual build process.
 *
 *
 * + Plugins: it’s the place where you configure which plugins Webpack will use.
 *
 *   Plugins, on the other hand,  work at SYSTEM LEVEL, and usually work at the end of the bundle or chunk generation process.
 *   Plugins can also modify how the bundles themselves are created. Plugins have more powerful control than loaders.
 *
 *   Plugins can deeply integrate into webpack because they can register hooks within webpacks build
 *   system and access (and modify) the compiler, and how it works, as well as the compilation.
 *   Therefore, they are more powerful, but also harder to maintain.
 *
 *
 * @type {webpack}
 * ********************************************************************************************************************/


// Node does not support ES6 import syntax at the moment. Use CommonJS require syntax in the meanwhile.
const webpack                   = require('webpack');
const path                      = require("path");
const HtmlWebPackPlugin         = require("html-webpack-plugin");
const MiniCssExtractPlugin      = require("mini-css-extract-plugin");
const {CleanWebpackPlugin}      = require('clean-webpack-plugin');
const CopyWebpackPlugin         = require('copy-webpack-plugin');
const HtmlWebpackPartialsPlugin = require("html-webpack-partials-plugin");
const ExtraWatchWebpackPlugin   = require('extra-watch-webpack-plugin');

/** App routes: Relative to the App root dir. */
const devPath   = './tmp';
const prodPath  = './dist';



// The externals configuration option provides a way of excluding dependencies from the output bundles. For example, to
// include jQuery from a CDN instead of bundling it, we will include the <script src=""/> on the html file , and set the
// module jquery to be excluded, and, in order to replace this module, the value jQuery will be used to retrieve a
// global jQuery variable.
// const externals = {
//     jquery: 'jQuery',
// };

// If you want to change the behavior according to the mode variable inside the webpack.config.js,
// you have to export a function instead of an object. When you export a function, the function will be invoked with
// 2 arguments: an environment as the first parameter and an options map as the second parameter.
// WEbPack 4 module.exports need to be a function ir order to use environment parameters.
module.exports = (env, options) => {

    /** Boolean to differentiate development or production mode in code. */
    const devMode = options.mode !== 'production';
    console.log('\x1b[36m%s\x1b[0m', "-----------------------------------------------------------------------------------------------");
    console.log('\x1b[36m%s\x1b[0m', `This is the Webpack 5 'mode': ${options.mode}, writing output to: ${devMode ? devPath : prodPath}`);
    console.log('\x1b[36m%s\x1b[0m', "-----------------------------------------------------------------------------------------------");

    return {

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // SOURCE CODE /////////////////////////////////////////////////////////////////////////////////////////////////////
        // The base directory, an absolute path, for resolving entry points and loaders from configuration. By default
        // the current directory is used, but it's recommended to pass a value in your configuration.
        // This makes your configuration independent from CWD (current working directory).
        context: __dirname + "/src",
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // SOURCE MAPS /////////////////////////////////////////////////////////////////////////////////////////////////////
        // When you build for production, along with minifying and combining your JavaScript files, you generate a source
        // map which holds information about your original files. When you query a certain line and column number in your
        // generated JavaScript you can do a lookup in the source map which returns the original location. Developer tools
        // (currently WebKit nightly builds, Google Chrome, or Firefox 23+) can parse the source map automatically and make
        // it appear as though you're running un-minified and uncombined files
        devtool: devMode ? 'eval-source-map' : false,
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // STATS ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // The stats option lets you precisely control what bundle information gets displayed. This can be a nice middle
        // ground if you don't want to use quiet or noInfo because you want some bundle information, but not all of it.
        stats: 'detailed',
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // CODE OPTIMIZATION: WebPack 4 code optimizations configuration. //////////////////////////////////////////////////
        optimization: {

            // This configuration object represents the default behavior of the SplitChunksPlugin.
            // It will create reusable chunks with common js and css code from several modules. For instance, if we have
            // 2 modules: login and index. It will split its js in index.js, index~login.js and vendors~index~login.js,
            // instead of putting all the code inside index.js. The last 2 files will be reused by login module, and as
            // a result the files index.js and login.js will only content the particular module js code.
            // This indicates which chunks will be selected for optimization. When a string is provided, valid values
            // are all, async, and initial. Providing all can be particularly powerful, because it means that chunks
            // can be shared even between async and non-async chunks.
            splitChunks: {
                minSize: 30000,
                maxSize: 500000,
            },
            // When using any devtool strategy that uses eval, such as cheap-module-eval-source-map, the browser developer
            // tools show two sets of files with quasi-identical names. The first set under the webpack:// origin contains
            // original sources. The second set under webpack-internal:// origin contains transpiled sources.
            // This is a hack required for Chrome. Elsewise breakpoints doesn't work in Chrome.
            // With the following line the file under webpack-internal:// will be named using a number, not the module name
            // so it will be clearer to open the right file on chrome dev-tools.
            moduleIds: 'named'
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // WATCH ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Turn on/off watching mode.
        watch: devMode,
        // Watch options.
        watchOptions: {
            ignored: /node_modules/,
            aggregateTimeout: 200 // Add a delay before rebuilding once the first file changed. This allows webpack to
            // aggregate any other changes made during this time period into one rebuild.
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // ENTRIES /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // The point or points to enter the application. At this point the application starts executing.
        // Simple rule: one entry point per HTML page --> SPA: one entry point, MPA: multiple entry points.
        entry: {
            index:          './assets/js/index.js'
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // OUTPUTS /////////////////////////////////////////////////////////////////////////////////////////////////////////
        output: {
            // This option determines the name of each output bundle.
            // The bundle is written to the directory specified by the output.path option.
            filename: devMode ? '[name].js' : '[name].[hash:6].js',
            // The output will be devPath when running build:dev and prodPath when running
            // build:prod. In the second case './dist' will also be copied to '../castorweb/src/dist/webapps/AQSAPP_TEMPLATE/dist'
            path: path.resolve(__dirname, devMode ? devPath : prodPath),
            // This option specifies the public URL of the output directory when referenced in a browser.
            // A relative URL is resolved relative to the HTML page (or <base> tag). Server-relative URLs,
            // protocol-relative URLs or absolute URLs are also possible and sometimes required, i. e. when
            // hosting assets on a CDN.
            // The value of the option is prefixed to every URL created by the runtime or loaders.
            // Because of this the value of this option ends with / in most cases.
            publicPath: './',
            // By default, asset/resource modules are emitting with [hash][ext][query] filename into output directory.
            // You can modify this template by setting output.assetModuleFilename in your webpack configuration:
            assetModuleFilename: devMode ? "./img/[name].[ext]" : "./img/[hash][ext][query]"
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // WEBPACK DEV SERVER CONFIGURATION ////////////////////////////////////////////////////////////////////////////////
        // Provides live reloading. This should be used for development only.
        // It uses webpack-dev-middleware under the hood, which provides fast in-memory access to the webpack assets.
        // When running build:dev:prod script from packages.json we can start a hot server that will push changes on
        // the app to our browser without need to reload the site.
        // The server will be listening on http://localhost:5000/
        devServer: {
            hot: true,
            contentBase: devPath,
            watchContentBase: true,
            compress: true,
            port: 5000,
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MODULES /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // In modular programming, developers break programs up into discrete chunks of functionality called a module.
        // Each module has a smaller surface area than a full program, making verification, debugging, and testing trivial.
        // Node.js has supported modular programming almost since its inception. On the web, however, support for modules
        // has been slow to arrive. WebPack applies the concept of modules to any file in your project..
        // In contrast to Node.js modules, webpack modules can express their dependencies in a variety of ways.
        // + An ES2015 import statement
        // + A CommonJS require() statement
        // + An AMD define and require statement
        // + An @import statement inside of a css/sass/less file.
        // + An image url in a stylesheet (url(...)) or html (<img src=...>) file.
        module: {

            // Prevent webpack from parsing any files matching the given regular expression(s). Ignored files should not
            // have calls to import, require, define or any other importing mechanism.
            // This can boost build performance when ignoring large libraries.
            // noParse: (content) => /jquery/.test(content),


            // An array of Rules which are matched to requests when modules are created. These rules can modify how the
            // module is created. They can apply loaders to the module, or modify the parser.
            // Loaders work at the individual file level during or before the bundle is generated.
            rules: [

                // PROCESS HTML FILES.
                // This loader will process all html files. Mainly it perform three tasks:
                //
                //  + Processing imgs paths
                //    By default every local <img src="image.png"> is required (require('./image.png')). You may need to
                //    specify loaders for images in your configuration (recommended file-loader or url-loader).
                //
                //    You can specify which tag-attribute combination should be processed by this loader via the query
                //    parameter attrs. Pass an array or a space-separated list of <tag>:<attribute> combinations.
                //    The defailt behaviour is --> attrs=img:src
                //
                //  + You can use interpolate flag to enable interpolation syntax for ES6 template strings:
                //    Allow us to interpolate other html files like head.html by doing ${require('../assets/inc/head.html')},
                //    <img src="${require(`./images/gallery.png`)}">, or <div>${require('./components/gallery.html')}</div>.
                //    ** interpolate attribute is not available since html-loader v1.0.0
                //
                //  + It will also minify the html code.
                {
                    test: /\.html$/,
                    use: [{
                        loader: "html-loader", options: {
                            minimize: !devMode,
                            //interpolate: true               //  To enable interpolation syntax for ES6 template strings, like so: <div>${require('./components/gallery.html')}</div>
                        }
                    }]
                },

                // PROCESS JS FILES
                // Transcript .js files from ECMA6 to ECMA5 using babel.
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },

                // PROCESS IMG FILES: https://webpack.js.org/guides/asset-modules/
                // Prior to webpack 5 it was common to use:
                //
                // + raw-loader to import a file as a string
                // + url-loader to inline a file into the bundle as a data URI
                // + file-loader to emit a file into the output directory
                //
                // Asset Modules type replaces all of these loaders by adding 4 new module types:
                //
                // + asset/resource --> emits a separate file and exports the URL. Previously achievable by using file-loader.
                // + asset/inline   --> exports a data URI of the asset. Previously achievable by using url-loader.
                // + asset/source   --> exports the source code of the asset. Previously achievable by using raw-loader.
                // + asset          --> webpack will automatically choose between resource and inline by following a default condition:
                //                      a file with size less than 8kb will be treated as a inline module type and resource module type otherwise.
                //                      Rule.parser.dataUrlCondition.maxSize option can be used to change size limit.
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    type: 'asset',
                    parser: {
                        dataUrlCondition: {
                            maxSize: 10 * 1024 // 10kb
                        }
                    }
                },

                // Load style sheets: SCSS files will be converted to CSS using sass-loader and compiled to a single
                // file styles.css by MiniCssExtractPlugin.
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {loader: 'css-loader', options: {url: true, sourceMap: devMode}},
                        {loader: 'sass-loader', options: {sourceMap: devMode}}
                    ]
                },

                // Load fonts: Only used fonts will be copied.
                {
                    test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
                    type: 'asset/resource',
                    generator: {
                        filename: devMode ? './fonts/[name][ext]' : './fonts/[hash][ext][query]'
                    }
                },

                // Copy license.md: Must be imported by some module.
                {
                    test: /(license)+(.md)$/,
                    type: 'asset',
                    generator: {filename: '[name][ext]'}
                },

                // Before Asset Modules and Webpack 5, it was possible to use inline syntax with the legacy loaders mentioned above.
                // It is now recommended to remove all inline loader syntax and use a resourceQuery condition to mimic the functionality of the inline syntax.
                {
                    resourceQuery: /file/,
                    type: 'asset/resource',
                    generator: {
                        filename: '[hash][ext][query]'
                    }
                }

            ]
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // PLUGINS /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Plugins work at bundle or chunk level and usually work at the end of the bundle generation process.
        // Plugins can also modify how the bundles themselves are created. Plugins have more powerful control than loaders.
        // A webpack plugin is a JavaScript object that has an apply method. This apply method is called by the webpack
        // compiler, giving access to the entire compilation lifecycle.
        plugins: [
            // CleanWebpackPlugin 3.0.0 --> Clean dist folder every build.
            new CleanWebpackPlugin(devMode
                // On development mode just erase the output.path folder content: ./tmp/
                ? {verbose: true, cleanStaleWebpackAssets: false}
                // On production mode.
                : { cleanOnceBeforeBuildPatterns: [
                        // Every file inside ./dist folder
                        '**/*',
                        // Remove directories tmp and dist inside castorweb/webapps/b2b.
                        path.join(process.cwd(), devPath + '/**/*')],
                    // Must set dry to false not to simulate deletion).
                   dry: false, verbose: true}),


            // Copy static folders or files.
            new CopyWebpackPlugin({
                patterns:[
                    //{ from: 'assets/images/food_sales/slider_closed_terminal',  to: 'slider_closed_terminal' },
                    //{ from: 'assets/images/food_sales/slider_open_terminal',    to: 'slider_open_terminal' },
                    { from: 'assets/locales',    to: 'assets/locales' } // Language resources used by i18next.
                ],
                options:{ }}),


            // The webpack compiler can understand modules written as ES2015 modules, CommonJS or AMD.
            // However, some third party libraries may expect global dependencies (e.g. $ for jQuery).
            // The libraries might also create globals which need to be exported. These "broken modules" a
            // re one instance where shimming comes into play.
            // ProvidePlugin: The ProvidePlugin makes a package available as a variable in every module compiled through
            // webpack. If webpack sees that variable used, it will include the given package in the final bundle.
            // Let's go ahead by removing the import statement for jquery and instead providing it via the plugin:
            new webpack.ProvidePlugin({
                $: "jquery",
                'jQuery': "jquery",
                'window.jQuery': 'jquery',
            }),


            // The DefinePlugin allows you to create global constants which can be configured at compile time.
            new webpack.DefinePlugin({
                DEVELOPMENT: JSON.stringify(devMode),
            }),

            // CSS files.
            // This plugin extracts CSS into separate files. It creates a CSS file per JS file which contains all CSS
            // imported by the corresponding entry point.
            // Since our CSS is general for all sections, we could create a single styles.css files that will be imported
            // in very section. But it generates an error on build, because every JS entry point will try to create the
            // file. If any of the processes try to access the file during the same itme the build will fail. That is why
            // we decided to create a different CSS file for each each JS entry point. On development we do not include
            // the hash on the name since we can configure no-cache on our browser. On production names will include a
            // hash so files will be updated and not cached since they have a new file name.
            new MiniCssExtractPlugin({
                filename:        devMode ? '[name]_styles.css' : '[name]_styles.[hash:6].css',
                chunkFilename:   devMode ? '[name]_styles.css' : '[name]_styles.[hash:6].css'
            }),
            // HTML files.
            // The HtmlWebpackPlugin simplifies creation of HTML files to serve your webpack bundles. The plugin will
            // generate an HTML5 file for you that includes all your webpack bundles in the body using script tags.
            new HtmlWebPackPlugin({
                title: 'AQS App Template',                  // This is a custom property we added to the config object, the value of this property would be used to embed in the placeholder element <%= htmlWebpackPlugin.options.title %> we have in our template html file.
                template: "./sections/index.html",          // Which file to use as template for the login.html being created.
                filename: "index.html",                     // The file to write the HTML to. Defaults to index.html. You can specify a subdirectory here too (eg: assets/admin.html)
                meta: {author: 'ÁngelBlanco'},              // Allows to inject meta-tags. E.g. meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'}
                chunks: ['index'],                          // Allows you to add only some chunks (e.g only the unit-test chunk)
                inject: 'head',                             // true || 'head' || 'body' || false --> Inject all assets into the given template or templateContent. When passing true or 'body' all javascript resources will be placed at the bottom of the body element. 'head' will place the scripts in the head element
                hash: true,                                 // If true then append a unique webpack compilation hash to all included scripts and CSS files. This is useful for cache busting
                minify: devMode ? false : {
                    collapseWhitespace: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true
                },                                          // Controls if and in what ways the output should be minified --> Default configuration if mode is 'production', otherwise false. (It removes redundant attrs like type="text" from inputs.
                cache: devMode,                             // Emit the file only if it was changed
                showErrors: devMode,                        // Errors details will be written into the HTML page.
                xhtml: true,                                // If true render the link tags as self-closing (XHTML compliant)
            }),

            // PARTIALS CONFIGURATION
            new HtmlWebpackPartialsPlugin([
                // Inject head.html inside <head> tag.
                {   path: path.join(__dirname, './src/assets/inc/head.html'),
                    priority: 'high',
                    location: 'head',
                    template_filename:'*'
                },
                // The nav_bar.html content will be injected whenever the plugin finds a <navigation> tag.
                {   path: path.join(__dirname, './src/assets/inc/nav_bar.html'),
                    location: 'navigation',
                    template_filename:'*'
            },
                // The footer.html content will be injected whenever the plugin finds a <footer> tag.
                {   path: path.join(__dirname, './src/assets/inc/footer.html'),
                    location: 'footer',
                    template_filename:'*'
            }]),

            // Attach extra files or dirs to webpack's watch system. Without this plugin, webpack wont watch for changes
            // in partials such as nav_bar.html
            new ExtraWatchWebpackPlugin({
                //files: [ 'path/to/file', 'src/**/*.json' ],
                dirs: [ path.join(__dirname, './src/assets/inc/') ],
            }),

            // INJECT FAVICON IN EVERY HTML FILE.
            // The default configuration will automatically generate webapp manifest files along with 44 different icon
            // formats as appropriate for iOS devices, Android devices, Windows Phone and various desktop browsers out
            // of your single favicon image.
            // new FaviconWebpackPlugin(path.join(__dirname, './src/assets/images/favicon/'))
        ]
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
};