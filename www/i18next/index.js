/**
 * Run this script by:
 * $ npm start
 *      or
 * $ node index.js
 *
 * This script will the the following:
 * + Will copy locales translation.json files from /src/asets/locales/** all langages wil be copied to i18next/locales/
 * + Will open the default language translation. Default language will be the first element on appLanguages array.
 * + Will store all default language keys and will check that they exist in each one of the rest of languages from appLanguages.
 * + If the key does no exists in any language it will create the key with the string {{****TRANSLATE (this must be deleted)****}} followed by the value within the default language.
 * + At last, it will show an HTML table sum-up with the missing keys that has just been created.
 *
 *
 * @type {e | (() => Express)}
 */

const express = require('express')
const i18next = require('i18next')
const Backend = require('i18next-http-backend'); // USE HTTP BACKEND
const editJsonFile = require("edit-json-file");
const fs = require('fs');
const ncp = require('ncp').ncp;
const rimraf = require("rimraf");

// Define available languages, default language must be first item.
const appLanguages = ['dev', 'en', 'es', 'fr'];

// Use express app.
const app = express()
app.disable("x-powered-by");
const port = process.env.PORT || 8080

// Var to store the missing keys
let missingKeys = [];
class Key { constructor (key, value, lang) { this.key = key; this.value = value; this.lang = lang;}}



/**
 * This method returns an array with all the keys for a json object, separated by '.'.
 *
 * @param obj
 * @param prefix
 * @returns {*[]}
 */
const keyify = (obj, prefix = '') =>
    Object.keys(obj).reduce((res, el) => {
        if( Array.isArray(obj[el]) ) {
            return res;
        } else if( typeof obj[el] === 'object' && obj[el] !== null ) {
            return [...res, ...keyify(obj[el], prefix + el + '.')];
        }
        return [...res, prefix + el];
    }, []);

/** Array of colors to apply to selected datatables rows. */
const colorsArray = ['#cff5fe', '#ffe6f9', '#ecffb8', '#efdeff', '#ffeddc', '#dcecff', '#cdf7e2', '#fff3d4'];
/** Generator method to loop throw colors array with no end. */
function *nextColor() {while(true) {yield* colorsArray}}



// Copy translation files from app to i18next folder.
rimraf.sync("./locales/"); // Delete any files that might be under ./i18next/locales/**

// Represents the number of pending file system requests at a time.
ncp.limit = 16;
// Copy nested files in folder --> ncp(source, destination, callback)
ncp('../src/assets/locales/', './locales', err => {
    if (err) { console.error(err)}
    console.log('Folders copied!');
    // Make sure that there is one directory for each language
    appLanguages.forEach( lang => {
        if (!fs.existsSync(`./locales/${lang}/`)) fs.mkdir(`./locales/${lang}/`, err => {if (err) console.log(err)})
    });

    // Start scanning files to find missing strings.
    startScan();
});


/**
 * This method will scan for missing translations on our app ./src/assets/locales/** folder.
 * It will take the appLanguages[0] as the default translation and will check that every other files contains the same
 * strings.
 *
 */
function startScan() {
    // Init i18next
    i18next.use(Backend).init({
        debug: false,
        preload: appLanguages,
        lng: 'en',
        backend: {
            loadPath:  'http://localhost:8080/locales/{{lng}}/{{ns}}.json',
            addPath:  'http://localhost:8080/locales/add/{{lng}}/{{ns}}',
            parsePayload: function parsePayload(namespace, key, fallbackValue) {
                console.log("PARSE PAYLOAD!-------------------------------------------");

                let value = i18next.t(key, { lng: "en" })
                console.log(value);
                return {key: key, value: value}} //parse data before it has been sent by addPath
        },
        nonExplicitSupportedLngs: true,
        supportedLngs: appLanguages,
        fallbackLng: false,
        load: 'languageOnly',
        saveMissing: true,
        saveMissingTo: 'all',

    }, () => {

        // GET ENDPPOINT to loop throw all JSON file to check if all translation for the default languages en are
        // available in the rest of languages.
        app.get('/analyse', (req, res) => {

            console.log("CHECKING EXISTING KEYS...");

            missingKeys = [];
            let langs = [...appLanguages];              // Make a copy of all availabe languages.
            let defLang = langs.shift();                // Get default language form first array item and remove it from appLanguages

            // Get DEV translation as the default to check if the rest of translation do have all the corresponding strings.
            let defTranslationJSON = JSON.parse(fs.readFileSync(__dirname + `/locales/${defLang}/translation.json`));
            let existingKeys = keyify(defTranslationJSON);   // Existing keys on def language file.


            // Loop all available languages:
            langs.forEach( lang => {
                // Switch to corresponding language.
                i18next.changeLanguage(lang) // will not load that!!! assert it was preloaded
                // Loop all the translation keys for this languages so i19next will check if the key exists in the tranlation.
                existingKeys.forEach(key => {
                    //console.log(i18next.exists(key, { lng: lang }));
                    //console.log(req.t(key));
                    // If the key does not exist for the current language
                    if (!i18next.exists(key, { lng: lang })) {missingKeys.push(new Key(key, i18next.t(key, {lng: defLang}), lang))}
                });
            });


            let colorIterator = nextColor(); // Iterator to iterate throw common colorsArray.
            let styles = `<style>
                    body { background-color: lightcyan;}
                    ${langs.map(lang => `tr.${lang} {background-color: ${colorIterator.next().value};}`).join(" ")}
                    table {border: 1px solid darkgrey;}
                    th { font-size: 1.2rem;}
                    tr > td {padding: .5rem;}
                    td:nth-child(3) {font-weight: bold;}
                </style>`;
            let htmlResponse = `<head>${styles}</head><body><div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width: 90%;">
                <h1>THERE ARE ${missingKeys.length} MISSING KEYS</h1>
                <table style="min-width: 100%;">
                    <thead><th>KEY</th><th>DEF VALUE</th><th>LANG</th></thead>
                    <tbody>${missingKeys.map(k => `<tr class="${k.lang}"><td>${k.key}</td><td>${k.value}</td><td>${k.lang}</td></td></tr>`).join('')}</tbody>
                </table><small>*The strings were inserted on corresponding json files with its <strong>default language value: ${defLang}</strong></small></div></body>`;

            // For every lang (except the default) loop every missing keys to insert them in the rest of languages.
            langs.forEach( lang => {
                // Get corresponding json file from disk
                let file = editJsonFile(`${__dirname}/locales/${lang}/translation.json`);
                // Get missing strings just for the corresponding language
                let missing = missingKeys.filter( key =>  key.lang === lang);
                missing.forEach( key => { file.set(key.key, `{{****TRANSLATE (this must be deleted)****}}${key.value}`) }); // Set the missing value.
                file.save(); // Save the data to the disk

                // Add results to html response:
                htmlResponse = [htmlResponse.slice(0, htmlResponse.lastIndexOf("</div></body>")),
                    `<li><small>${missing.length} keys were added to /locales/${lang}/translation.json</small></li>`,
                    htmlResponse.slice(htmlResponse.lastIndexOf("</div></body>"))].join('');
            });


            // Send response with results summary.
            res.send(htmlResponse);
            res.end();
            process.exit(0); // Exit script with no errors.
        });


        // POST ENDPOINT to receive request when any translation for any language is missing in the corresponding
        // JSON file. This endpoint will not be used since we will scan all strings and add the missing ones from the GET
        // endpoint.
        app.post("/locales/add/:lang/:ns/", (req, res) => {

            // Get params form POST request.
            let lang = req.params.lang;
            let ns = req.params.ns;
            let key = req.body.key;
            let value = req.body.value;

            console.log(`MISSING KEY POST RECEIVED! => Include {${key}:${value}} for Lang => ${lang} & NS => ${ns}` )

            console.log( 'http://' + req.hostname + req.url);
            // Get the corresponding translation file from disk.
            let file = editJsonFile(`${__dirname}/locales/${lang}/${ns}.json`);
            file.set(key, value); // Set the missing value.

            // Save the data to the disk
            file.save();
        });

    });


    // To serve static files
    app.use(express.static("."));

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONTENT TYPE ////////////////////////////////////////////////////////////////////////////////////////////////////
    // This makes express use the body parser to parse into an object the request body's content.
        app.use(express.json());                            // to support JSON-encoded bodies
        app.use(express.urlencoded({extended: true}));      // to support URL-encoded bodies
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    app.listen(port, () => {
        console.log(`Server is listening on port ${port}`)
        console.log(`Go to http://localhost:8080/ to start running the application...`)
    })

}


