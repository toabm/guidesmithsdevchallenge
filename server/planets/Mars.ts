import {Position} from "../common/models/Position";
import {Robot} from "../robots/Robot"

export class Mars {

    get xmax() {return this._xmax;}
    set xmax(value) {this._xmax = value;}
    get ymax() {return this._ymax;}
    set ymax(value) {this._ymax = value;}
    get xmin() {return this._xmin;}
    set xmin(value) {this._xmin = value;}
    get ymin() {return this._ymin;}
    set ymin(value) {this._ymin = value;}
    get scents() {return this._scents;}
    set scents(value) {this._scents = value;}

    // Max value of any (X,Y).
    maxValueX = 50;
    maxValueY = 50;

    _xmin:number;
    _ymin:number;
    _scents: Position[];
    _xmax:number;
    _ymax:number;
    exploredArea:number;
    exploredCoords: Position[][];
    robotsArray:Robot[];

    /**
     *
     * @param xmax
     * @param ymax
     */
    constructor(xmax:number, ymax:number) {
        this._xmin = 0;
        this._ymin = 0;
        this._scents = [];
        // Check if planet size does not exceed the max values allowed.
        this._xmax = xmax <= this.maxValueX ? xmax : this.maxValueX;
        this._ymax = ymax <= this.maxValueY ? ymax : this.maxValueY;
        this.exploredArea = 0;
        this.exploredCoords = [];
        this.robotsArray = [];
    }

    /**
     *
     * @param position
     */
    addScent = (pos:Position) => this._scents.push(pos);


    hasScent = (pos:Position) => { return this._scents.filter(s => s.equals(pos)).length > 0};

    /**
     *
     * @param position
     * @returns {boolean}
     */
    isOutSpace = (position:Position) => {return position.x > this._xmax || position.y > this._ymax};


    /**
     *
     * @returns {number}
     */
    increaseExploredArea = (pos:Position = new Position(-1,-1)) => {
        this.exploredArea++;
    };


    /**
     *
     * @param robot
     */
    addRobot = (robot:Robot) => {this.robotsArray.push(robot)}


    getStat = () =>
        this.robotsArray.map( r => `${r.x} ${r.y} ${r.o} ${r.isLost ? 'LOST' : ''}`).join("\n");


}

