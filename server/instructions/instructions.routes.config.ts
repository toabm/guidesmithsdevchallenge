import { CommonRoutesConfig } from '../common/common.routes.config';
import InstructionsController from './controllers/instructions.controller';
import InstructionsMiddleware from './middleware/instructions.middleware';
import express from 'express';

export class InstructionsRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'InstructionsRoutes');
    }

    configureRoutes(): express.Application {
        this.app
            .route(`/instructions`)
            .get(InstructionsController.listInstructions)
            // CREATE NEW USER
            .post(
                InstructionsMiddleware.validateRequiredInstructionsBodyFields,
                InstructionsMiddleware.validateInstructionsFormat,
                InstructionsController.createInstruction
            );

        return this.app;
    }
}

