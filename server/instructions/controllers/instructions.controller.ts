/**
 * The idea behind controllers is to separate the route configuration from the code that finally processes a route request.
 *
 * That means that all validations should be done before our request reaches the controller. The controller only needs to
 * know what to do with the actual request because if the request made it that far, then we know it turned out to be valid.
 * The controller will then call the respective service of each request that it will be handling.
 */

// We import express to add types to the request/response objects from our controller functions
import express from 'express';
// We import our newly created instructions services
import instructionsService from '../services/instructions.service';
// We import the argon2 library for password hashing
import argon2 from 'argon2';
// We use debug with a custom context as described in Part 1
import debug from 'debug';
import {CreateInstructionDto} from "../dto/create.instruction.dto";

// Input models
import {Mars} from '../../planets/Mars'
import {Robot} from "../../robots/Robot";
import {Position} from "../../common/models/Position";

// Debug exposes a function; simply pass this function the name of your module,
// and it will return a decorated version of console.error for you to pass debug statements to
const log: debug.IDebugger = debug('app:instructions-controller');


class InstructionsController implements CreateInstructionDto {

    // Max length for any instruction input
    maxLength = 1000;

    input: string;
    output: string;
    infoPerRobot: Object[];
    scents: Position[];
    planetWidth: number;
    planetHeight: number;

    /**
     * Constructor class.
     * @param input
     * @param output
     * @param infoPerRobot
     * @param scents
     */
    constructor(input:string = '', output:string = '', infoPerRobot:Object[] = [], scents: Position[] = []) {
        this.input = input;
        this.output  = output;
        this.scents = scents;
        this.planetHeight = 0;
        this.planetWidth = 0;
        this.infoPerRobot = infoPerRobot;
    }


    /**
     * Since InstructionsController follows singleton pattern, we need this method to clean the instance every time
     * we receive a new input.
     */
    clear() {
        this.input = "";
        this.output  = "";
        this.infoPerRobot = [];
        this.scents = [];
        this.planetHeight = 0;
        this.planetWidth = 0;
    }

    /**
     *
     */
    getLines = () => this.input.split(/\n/);


    /**
     *
     */
    calculateOutput = () => {

        // Read instructions lines
        let lines = this.getLines();

        //  First line declare the planet: Create Planet:
        let planetWidth = lines[0].split(" ")[0];
        let planetHeight = lines[0].split(" ")[1];
        let planet = new Mars( parseInt(planetWidth),  parseInt(planetHeight));

        // Extract 1st line from array
        lines.shift();
        // Loop the lines to create robots and give them instructions.
        lines.forEach((currentValue:string, index:number, array:string[]) => {
            // Odd lines will be robot definitions
            if (index % 2 == 0) {
                log(`New Robot =============> Robot definition: ${currentValue}`);
                let robot = new Robot(parseInt(currentValue.split(" ")[0]), parseInt(currentValue.split(" ")[1]), currentValue.split(" ")[2], planet, currentValue.includes("LOST"));
                planet.addRobot(robot); // Push into robots array.
            }
            // Even lines will store previous robot orders.
            else {
                let robot = planet.robotsArray[planet.robotsArray.length -1]
                log(`Robot orders: ${currentValue}`);
                [...currentValue].forEach(o => {
                    robot.runOrder(o);
                });
                // After running all robot orders, save the info for this robot inside this.infoPerRobot array. We will
                // store on object per robot, informing if the robot was lost, the position it explored, etc.
                this.infoPerRobot.push({isLost: robot.isLost, exploredPosition: robot.exploredPositions});
            }
        });

        // Fill the Instruction attributes calculated from input.
        this.output = planet.getStat()
        this.scents = planet.scents;
        this.planetWidth = planet.xmax;
        this.planetHeight = planet.ymax;

    }



    listInstructions = async(req: express.Request, res: express.Response) => {
        const ins = await instructionsService.list(100, 0);
        res.status(200).send(ins);
    }

    /**
     * This method will store a new instruction object in ddbb.
     *
     * It will first calculate the output string and the explored Positions for each robot from the input param in request body.
     * @param req
     * @param res
     */
    createInstruction = async (req: express.Request, res: express.Response) => {
        // Since Instruction class is a singleton we must clean previous generated data:
        this.clear();

        // In the body request we only have the input. The controller will calculate rest of the info
        this.input = req.body.input;
        // Calculate output and inject Instruction attributes in body request before saving on DDBB.
        this.calculateOutput();
        req.body.output = this.output;
        req.body.scents = this.scents;
        req.body.planetWidth = this.planetWidth;
        req.body.planetHeight = this.planetHeight;
        req.body.infoPerRobot = this.infoPerRobot;
        const instruction = await instructionsService.create(req.body);
        log(`New instructions set stored on ddbb => ${instruction}`);
        res.status(201).send( instruction );
    }


}

export default new InstructionsController();