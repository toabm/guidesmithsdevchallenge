import express from 'express';
import debug from 'debug';
import {Robot} from "../../robots/Robot";
import InstructionsController from '../controllers/instructions.controller';

const log: debug.IDebugger = debug('app:users-controller');


class InstructionsMiddleware {



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // VALIDATIONS /////////////////////////////////////////////////////////////////////////////////////////////////////



    async validateRequiredInstructionsBodyFields(req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.body && req.body.input) {
            next();
        } else {
            res.status(400).send({
                error: `Missing required fields input!!`,
            });
        }
    }


    /**
     *
     * @param req
     * @param res
     * @param next
     */
    async validateInstructionsFormat(req: express.Request, res: express.Response, next: express.NextFunction) {

        // Get instructions.
        let instructions = req.body.input;
        // Split lines to treat the separately
        let lines = instructions.split("\n");

        if ((lines.length % 2) !== 1) return false;  // There is an even number of lines.
        if (lines.length < 3) return false; // Minimum number of lines is 3
        if (lines[0].split(" ").length !== 2)  return false; // First line must contain 2 number.
        lines.shift(); // Remove frist line and validate the rest.
        let correctOrders = true;
        lines.forEach( (currentValue:string, index:number) => {
            // Odd lines will be robot definitions
            if (index % 2 == 0) {
                if (currentValue.split(" ").length !== 3 // First line for every robot must contain 3 values: 2 numbers and one orientation
                    || parseInt(currentValue.split(" ")[0]) > 50 || parseInt(currentValue.split(" ")[1]) > 50 // Initial robot coords max value is 50.
                    || Robot.orientations.filter( elem => elem === [...currentValue][currentValue.length - 1]).length === 0 ) correctOrders = false; // The last char must exists in Robot.orientations Array.
            // Even lines will be robot orders.
            } else {
                if (currentValue.split(" ").length !== 1 ||                                // Check there are no spaces in orders line.
                    ![...currentValue].every( elem => Robot.orders.includes(elem)) ||               // Check that every order is one of Robot.orders array
                    currentValue.length > InstructionsController.maxLength) correctOrders = false;  // Check line doesnt exceed maxLength

            }
        });

        if (!correctOrders) {
            res.status(400).send({ error: `There is a problem with instructions format!` });
        } else {
            next();
        }
    }





}

export default new InstructionsMiddleware();