/**
 * DTO contains just the fields that we want to pass between the API client and our database.
 */
import {Position} from "../../common/models/Position";

export interface CreateInstructionDto {
    // id: string; // Mongoose automatically makes an _id field available
    input: string;
    output: string;
    infoPerRobot: Object[],
    scents: Position[];
    planetWidth: Number;
    planetHeight: Number;
}