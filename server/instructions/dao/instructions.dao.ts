// Import connection to MongoDB.
import mongooseService from '../../common/services/mongoose.service';
import debug from 'debug';
import shortid from "shortid";
// Import DTOs.
import { CreateInstructionDto }    from '../dto/create.instruction.dto';
// Set debugger
const log: debug.IDebugger = debug('app:instructions-dao');


class InstructionsDao {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // INSTRUCTIONS SCHEMA FOR MONGOOSE ////////////////////////////////////////////////////////////////////////////////
    // The Instructions schema defines which fields should exist in our MongoDB collection called Instructions,
    // while the DTO entities defines which fields to accept in an HTTP request.
    Schema = mongooseService.getMongoose().Schema;
    instructionsSchema = new this.Schema({
        _id: String,
        input: String,
        output: String,
        infoPerRobot: Array,
        scents: Array,
        planetWidth: Number,
        planetHeight: Number
    }, { id: false }); // Mongoose models provide a virtual id getter by default, so we�ve disabled that option above with { id: false }

    Instruction = mongooseService.getMongoose().model('Instructions', this.instructionsSchema);
    // END OF INSTRUCTIONS SCHEMA FOR MONGOOSE /////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Class constructor
     */
    constructor() {log('Created new instance of InstructionsDAO')}


    /**
     * CREATE INSTRUCTIONS
     *
     * This method will store a new Instruction object in ddbb.
     *
     * @param instructionFields The newly created instruction.
     */
    async addInstruction(instructionFields: CreateInstructionDto) {
        const id = shortid.generate(); // Generate ID.
        // Instantiate schema.
        const instruction = new this.Instruction({_id: id, ...instructionFields});
        await instruction.save();
        return instruction; // Return newly created instruction.
    }


    // GET INSTRUCTIONS
    async getInstructions(limit = 25, page = 0) {
        return this.Instruction.find().limit(limit).skip(limit * page).exec();}


    // GET INSTRUCTION BY ID
    async getInstructionById(instructionsId: string) {
        return this.Instruction.findOne({ _id: instructionsId }).populate('Instruction').exec();
    }

    // DELETE INSTRUCTION
    async removeInstructionById(instructionsId: string) {return this.Instruction.deleteOne({ _id: instructionsId }).exec()}


}


// Using the singleton pattern, this class will always provide the same instance
export default new InstructionsDao();