import InstructionsDao from '../dao/instructions.dao';
import { CRUD } from '../../common/interfaces/crud.interface';
import { CreateInstructionDto } from '../dto/create.instruction.dto';


/**
 * Instructions Service class
 */
class InstructionsService implements CRUD {

    /**
     * Create new instruction.
     * @param resource
     */
    async create(resource: CreateInstructionDto) {
        return InstructionsDao.addInstruction(resource);
    }


    /**
     *  Passing limit and page parameters along to getUsers() for pagination purposes.
     * @param limit
     * @param page
     */
    async list(limit: number, page: number) {
        return InstructionsDao.getInstructions(limit, page);
    }

    /**
     *
     * @param id
     */
    async readById(id: string) {
        return InstructionsDao.getInstructionById(id);
    }


    /**
     *
     * @param id
     */
    async deleteById(id: string) {
        return InstructionsDao.removeInstructionById(id);
    }


}
export default new InstructionsService();