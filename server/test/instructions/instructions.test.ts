import app from '../../app';
import supertest from 'supertest';
import { expect } from 'chai';
import shortid from 'shortid';
import mongoose from 'mongoose';


const instructions = {input: `5 3\n1 1 E\nRFRFRFRF\n3 2 N\nFRRFLLFFRRFLL\n0 3 W\nLLFFFLFLFL`};


/**
 * The functions we�re passing to before() and after() get called before and after all the tests we�ll define by calling
 * it() within the same describe() block. The function passed to after() takes a callback, done,
 * which we ensure is only called once we�ve cleaned up both the app and its database connection.
 *
 * Note: Without our after() tactic, Mocha will hang even after successful test completion. The advice is often to
 * simply always call Mocha with --exit to avoid this, but there�s an (often unmentioned) caveat.
 * If the test suite would hang for other reasons�like a misconstructed Promise in the test suite or the app itself�then
 * with --exit, Mocha won�t wait and will report success anyway, adding a subtle complication to debugging.
 */
describe('Instructions endpoints tests:', function () {
    let request: supertest.SuperAgentTest;
    before(function () {request = supertest.agent(app)});
    after(function (done) {
        // Shut down the Express.js server, close our MongoDB connection, then tell Mocha we're done:
        app.close(() => {mongoose.connection.close(done)});
    });

    /**
     * Instructions POST endpoint test.
     */
    it('Should allow a POST to /instructions', async function () {
        const res = await request.post('/instructions').send(instructions);
        expect(res.status).to.equal(201);
        expect(res.body).not.to.be.empty;
        expect(res.body).to.be.an('object');
        expect(res.body._id).to.be.a('string');

    });

    /**
     * Instructions GET endpoint test.
     */
    it('Should allow a GET from /instructions', async function () {
        const res = await request.get(`/instructions`).send();

        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body).to.be.an('array');

    });

});