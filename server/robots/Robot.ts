import {Position} from "../common/models/Position";
import {Mars} from "../planets/Mars";
import debug from "debug";
// Debug exposes a function; simply pass this function the name of your module,
// and it will return a decorated version of console.error for you to pass debug statements to
const log: debug.IDebugger = debug('app:Robot');
/**
 *
 */
export class Robot {


    x:number;
    y:number;
    o:string;
    isLost:boolean;
    planet:Mars;
    exploredArea:number;
    public exploredPositions: Array<Position>;

    /** Possible orientations: Clock wise */
    static orientations = ['N', 'E', 'S', 'W'];
    /** Var position to use coordinates. */
    //position = (x, y) => {return {x: x, y: y}}

    static orders = ['R', 'L', 'F']


    /**
     *
     * @param x         Position X
     * @param y         Position Y
     * @param o         Orientation
     * @param planet    The planet the robot is in: Presumably Mars instance.
     * @param isLost    Is lost scent (true | false)
     */
    constructor(x:number, y:number, o:string, planet:Mars, isLost:boolean = false) {
        this.x = x;
        this.y = y;
        this.o = o;
        this.isLost = isLost;
        this.planet = planet;
        this.exploredArea = 0;
        this.exploredPositions = [];

        // Include info from the starting position:
        this.planet.increaseExploredArea();
        this.exploredPositions.push(new Position(this.x, this.y));
    }


    /**
     *
     * @param o
     */
    runOrder = (o:string) => {
        if(this.isLost) {
            log("Order Ignored. => Robot is Lost");
            return;
        }
        // Run corresponding order.
        switch (o) {
            case 'L':
                this.turn.left();
                break;
            case 'R':
                this.turn.right();
                break;
            case 'F':
                this.moveForward();
                break;
            default:
                console.error("Order Not accepted!");
                break;
        }

    }

    /**
     * Methods to turn robot: Instructions 'L' & 'R'.
     * @type {{left: function(): string, right: function(): string}}
     */
    turn = {
        right: () => this.o = Robot.orientations[Robot.orientations.findIndex(elem => elem === this.o) + 1] || Robot.orientations[0],
        left:  () => this.o = Robot.orientations[Robot.orientations.findIndex(elem => elem === this.o) - 1] || Robot.orientations[Robot.orientations.length - 1],
    }

    /**
     * Method to move forward: Instruction 'F'
     */
    moveForward = () => {

        let newx = this.x, newy = this.y, newPos;

        switch (this.o) {
            case Robot.orientations[0]: // Move Nord
                newy++;
                break;
            case Robot.orientations[1]: // Move East
                newx++;
                break;
            case Robot.orientations[2]: // Move South
                newy--;
                break;
            case Robot.orientations[3]: // Move West
                newx--;
                break;
        }

        // New Robot position.
        newPos = new Position(newx,newy);

        // Check if new position inside boundaries.
        let isOutSpace = this.planet.isOutSpace(newPos);


        // Check if current position has an SCENT.
        let currentHasScent = this.planet.hasScent(new Position(this.x, this.y));

        // If newPos is inside planet boundaries we wil just move the robot.
        if (!isOutSpace) {
            this.x = newPos.x;
            this.y = newPos.y;
            this.exploredArea++; // Increase both the robot explored Area and the total explored area on the planet.
            this.planet.increaseExploredArea();
            this.exploredPositions.push(new Position(this.x, this.y));
            log(`Robot moved to (${this.x},${this.y}) heading ${this.o}!!`)
        }
        // The robot is suiciding: Next position is outside the planet but there is no scent on planet grid.
        else if (isOutSpace && !currentHasScent) {
            let keptPosition = new Position(this.x, this.y);
            if(this.planet.scents.filter(s => s.equals(keptPosition)).length === 0) this.planet.addScent(keptPosition); // Leave an Scent on the planet position.
            this.isLost = true; // Mark the robot as LOST.
            //this.x = newPos.x;  // Save robot out-space position.
            //this.y = newPos.y;
            log(`Robot lost at (${this.x},${this.y}): Scent Left!`)
        }

       // If robot is going to out-space but we found a scent in current Position, robot wont move.
       else if (isOutSpace && currentHasScent) {
            log(`Robot stopped at (${this.x},${this.y}): Scent Found! Suicide Avoided!`)
        }



    }


    // Stat getter
    get stat() {
        return { "PositionX": this.x, "PositionY": this.y, "Orientation": this.o, "IsLost": this.isLost, "ExploredArea": this.exploredArea};
    }


}
