export class Position {

    _x:number;
    _y:number;



    /**
     * Class constructor
     * @param x Coordinate X
     * @param y Coordinate Y
     */
    constructor(x:number = 0,y:number = 0) {
        this._x = x;
        this._y = y;
    }

    /**
     * Compare positions.
     * @param pos2
     * @returns {number}
     */
    equals (pos2:Position) {
        if (this._x === pos2._x && this._y === pos2._y) return true;
        else return false;
    }

    get x() {return this._x;}
    set x(value) {this._x = value;}
    get y() {return this._y;}
    set y(value) {this._y = value;}
}